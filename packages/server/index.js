const Fastify = require("fastify");
const fetch = require("node-fetch");

const app = Fastify({ logger: true });

app.register(require("fastify-cors"), {});

const getUrlEndpoint = (url) => {
  if (url.startsWith("https://www.flickr.com")) {
    return `https://www.flickr.com/services/oembed/?format=json&url=${encodeURIComponent(
      url
    )}`;
  }

  if (url.startsWith("https://vimeo.com")) {
    return `https://vimeo.com/api/oembed.json?url=${encodeURIComponent(url)}`;
  }

  return null;
};

app.get("/", async (req, rep) => {
  const endpoint = getUrlEndpoint(req.query.url);

  if (endpoint) {
    const response = await fetch(endpoint);

    if (response.status === 200) {
      return response.json();
    }
  }

  return rep
    .code(500)
    .header("Content-Type", "application/json; charset=utf-8")
    .send({
      code: response.status,
      error: `error trying to access ${req.query.url}`,
    });
});

app.listen(4000, "0.0.0.0", function (err, address) {
  if (err) {
    app.log.error(err);
    process.exit(1);
  }
  app.log.info(`server listening on ${address}`);
});

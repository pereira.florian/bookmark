import { useState, useCallback } from 'react';

const useLocalStorage = <S>(
  key: string,
  initialValue: Readonly<S | (() => S)>
) => {
  const [storedValue, setStoredValue] = useState<S>(() => {
    const item = window.localStorage.getItem(key);
    if (item) {
      try {
        return JSON.parse(item);
      } catch { }
    }

    const value =
      typeof initialValue === 'function'
        ? initialValue()
        : initialValue;

    return value;
  });

  const setValue = (value: Readonly<S | ((prev: S) => S)>) => {
    setStoredValue((prev: S) => {
      try {
        const next = typeof value === 'function'
          ? value(prev)
          : value;
        window.localStorage.setItem(key, JSON.stringify(next));

        return next;
      } catch { }

      return prev;
    });
  };

  const removeItem = useCallback(() => window.localStorage.removeItem(key), [key]);

  return [storedValue, setValue, removeItem] as const;
};


export default useLocalStorage;
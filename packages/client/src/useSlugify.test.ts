import { renderHook, act } from "@testing-library/react-hooks";
import useSlugify from "./useSlugify";

it("initial value", () => {
  const {
    result,
  } = renderHook(() => useSlugify("https://vimeo.com/176711714"));
  expect(result.current).toBe("https-vimeocom-176711714");
});

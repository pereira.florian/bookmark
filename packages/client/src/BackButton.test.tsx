import * as React from 'react';
import { render, cleanup } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import BackButton from './BackButton';

afterEach(() => {
  cleanup();
});

it("Render null", () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/']}>
      <BackButton />
    </MemoryRouter>
  );

  expect(container.firstChild).toBeNull();
});

it("Render button", () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/test-id/edit']}>
      <BackButton />
    </MemoryRouter>
  );

  expect(container.firstChild).toBeInstanceOf(HTMLButtonElement);
});

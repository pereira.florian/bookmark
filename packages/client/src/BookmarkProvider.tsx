import * as React from 'react';
import useLocalStorage from './useLocalStorage';

type ContextProps = {
  bookmarks: string[],
  add: (url: string) => Promise<void>,
  remove: (index: number) => Promise<void>,
};

const Context = React.createContext<ContextProps>({
  bookmarks: [],
  add: async () => undefined,
  remove: async () => undefined,
});

type Props = {
  children: React.ReactNode;
};

const BookmarkProvider: React.FC<Props> = ({ children }) => {
  const [bookmarks, setBookmarks] = useLocalStorage<string[]>('bookmarks', () => []);
  const add = React.useCallback(async (url) => {
    setBookmarks((prev: string[]) => {
      const next = [...new Set([url, ...prev])];
      return next;
    });
  }, []);

  const remove = React.useCallback(async (index) => {
    setBookmarks((prev: string[]) => prev.filter((_, i) => i !== index));
  }, []);

  return <Context.Provider value={{ bookmarks, add, remove }}>{children}</Context.Provider>;
};

export default BookmarkProvider;

export const useBookmarks = () => React.useContext(Context);



import * as React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import BookmarkProvider from './BookmarkProvider';
import Home from './Home';
import Edit from './Edit';
import BackButton from './BackButton';

const App = () => <div className="relative w-full sm:w-2/3 min-h-screen mx-auto p-8">
  <BookmarkProvider>
    <Router>
      <Title />
      <Switch>
        <Route path="/:id/edit" component={Edit} />
        <Route path="/:page" component={Home} />
        <Route path="/" component={Home} />
        <Redirect to="/" />
      </Switch>
    </Router>
  </BookmarkProvider>
</div>;

export default App;

const Title = () => (
  <div className="flex relative">
    <BackButton />
    <h1 className="text-2xl text-blue-400 font-semibold text-center mx-auto">Bookmark</h1>
  </div>
);
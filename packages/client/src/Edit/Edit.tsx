import React from 'react';
import { Redirect, useParams } from "react-router-dom";
import useLocalStorage from '../useLocalStorage';
import TagInput from './TagInput';
import ExternalLinkIcon from '../ExternalLinkIcon';

import type { Bookmark } from '../Bookmark';

const Edit = () => {
  let { id } = useParams<{ id: string; }>();
  const [bookmark, setBookmark] = useLocalStorage<Bookmark>(id, () => undefined);

  const handlePersistTag = React.useCallback((e) => {
    setBookmark((prev: Bookmark) => ({
      ...prev,
      tags: e.target.value || [],
    }));
  }, []);

  if (bookmark === undefined) {
    return <Redirect to="/" />;
  }

  return <>
    <Details bookmark={bookmark} />
    <TagInput onChange={handlePersistTag} value={bookmark.tags} />
  </>;
};

export default Edit;



type Props = {
  bookmark: Bookmark,
};

const Details: React.FC<Props> = ({ bookmark }) => (
  <div className="w-full flex flex-col text-gray-600 mb-6">
    <h1 className="text-base md:text-xl font-semibold my-8 flex justify-between">{bookmark.title}
      <a href={bookmark.url} target="_blank"><ExternalLinkIcon className="w-6 h-6 ml-auto" /></a>
    </h1>
    <div className="flex flex-col xl:flex-row space-y-6 lg:space-y-0 lg:space-x-8 text-sm italic">
      <div dangerouslySetInnerHTML={{ __html: bookmark.html }} />
      <div className="w-full">
        <div className="truncate"><strong>URL:</strong> {bookmark.url}</div>
        <div className="whitespace-nowrap truncate">
          <strong>Title:</strong> {bookmark.title}</div>
        {bookmark.duration && <div><strong>Duration:</strong> {bookmark.duration}</div>}
        <div><strong>Size: </strong> {bookmark.width} x {bookmark.height}</div>
        <div><strong>Author: </strong>{bookmark.author_name}</div>
        <div><strong>Added at:</strong> {bookmark.added_at}</div>
      </div>
    </div>
  </div>
);

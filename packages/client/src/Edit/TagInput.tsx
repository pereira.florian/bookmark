import React from 'react';
// @ts-ignore
import Tags from "@yaireo/tagify/dist/react.tagify";
import "@yaireo/tagify/dist/tagify.css";

type Props = {
  value: {
    value: string;
  }[],
  onChange: (e: React.SyntheticEvent<HTMLInputElement>) => void,
};

const Tag: React.FC<Props> = ({ onChange, value }) => {
  return (
    <Tags
      value={value}
      onChange={onChange}
      placeholder="Add tags"
    />
  );
};

export default Tag;
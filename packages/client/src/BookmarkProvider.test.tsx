import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BookmarkProvider, { useBookmarks } from "./BookmarkProvider";
import { renderHook, act } from '@testing-library/react-hooks';


test('Add & remove URL', () => {
  const wrapper = (props: { children: React.ReactNode; }) => <BookmarkProvider>{props.children}</BookmarkProvider>;
  const { result } = renderHook(() => useBookmarks(), { wrapper });

  act(() => {
    result.current.add("http://www.flickr.com/any/link");
  });

  expect(result.current.bookmarks).toEqual(["http://www.flickr.com/any/link"]);

  act(() => {
    result.current.remove(0);
  });

  expect(result.current.bookmarks).toEqual([]);
});
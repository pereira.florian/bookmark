
export type Bookmark = {
  id: string,
  type: "photo" | "video",
  url: string,
  title: string,
  author_name: string,
  added_at: string,
  width: number,
  height: number,
  html: string,
  thumbnail_url: string,
  thumbnail_width: number,
  thumbnail_height: number,
  duration?: number,
  tags: {
    value: string;
  }[],
};
import { renderHook, act } from "@testing-library/react-hooks";
import useLocalStorage from "./useLocalStorage";

it("initial value", () => {
  const {
    result,
  } = renderHook(() => useLocalStorage("test", () => "testValue"));

  const [state, setState] = result.current;
  expect(state).toBe("testValue");

  act(() => {
    setState("upValue");
  });

  expect(window.localStorage.getItem("test")).toBe(JSON.stringify("upValue"));
});

it("up value", () => {
  const {
    result,
  } = renderHook(() => useLocalStorage("test", () => "testValue"));

  const [_, setState] = result.current;

  act(() => {
    setState("upValue");
  });

  expect(window.localStorage.getItem("test")).toBe(JSON.stringify("upValue"));
});


it("remove value", () => {
  const {
    result,
  } = renderHook(() => useLocalStorage("test", () => "testValue"));

  const [_, __, remove] = result.current;

  act(() => {
    remove();
  });

  expect(window.localStorage.getItem("test")).toBeNull();
});

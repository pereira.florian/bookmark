import React from 'react';
import { useBookmarks } from '../../BookmarkProvider';
import FlickrVimeoInput from './FlickrVimeoInput';

type Props = {
  cancel: () => void,
};

const BookmarkForm: React.FC<Props> = ({ cancel }) => {
  const inputRef = React.useRef<HTMLInputElement>(null);
  const { add } = useBookmarks();

  const handleSubmit = (async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (inputRef.current === null) return;

    const url = new URL(inputRef.current.value);

    if (url.hostname.includes('flickr')) {
      add(`https://www.flickr.com${url.pathname}`);
    }

    if (url.hostname.includes('vimeo')) {
      add(`https://vimeo.com${url.pathname}`);
    }

    inputRef.current.value = "";
    cancel();
  });

  const handleCancel = React.useCallback((e: React.SyntheticEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (inputRef.current === null) return;
    inputRef.current.value = "";
    cancel();
  }, []);

  React.useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  return <form onSubmit={handleSubmit}>
    <FlickrVimeoInput ref={inputRef} />
    <div className="flex justify-between">
      <button type="submit" className="callToAction bg-green-600">
        <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
        </svg>
        Submit</button>
      <button key="toggle" type="button" className="callToAction bg-red-600" onClick={handleCancel}>
        <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
        </svg>
        Cancel
      </button>
    </div>
  </form>;
};

export default BookmarkForm;
import React from 'react';
import BookmarkForm from './BookmarkForm';


const NewBookmark = () => {
  const [open, setOpen] = React.useState(() => false);
  const toggle = React.useCallback(() => {
    setOpen((prev) => !prev);
  }, []);

  return open
    ? <BookmarkForm cancel={toggle} />
    : <NewButton onClick={toggle} />;
};

export default NewBookmark;


type Props = {
  onClick: () => void,
};

const NewButton: React.FC<Props> = ({ onClick }) => (
  <button type="button" onClick={onClick} className="callToAction bg-green-600">
    <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
    </svg>
    New Link
  </button>);
import React, { SyntheticEvent } from "react";
import flickrImg from './flickr.svg';
import vimeoImg from './vimeo.svg';

const FlickrVimeoInput = React.forwardRef<HTMLInputElement>((_, inputRef) => {
  const [state, setState] = React.useState<string | null>(() => null);
  const handleCheckLink = React.useCallback((e: SyntheticEvent<HTMLInputElement>) => {
    if (e.currentTarget.value.match(/http[s]*:\/\/[www.]*flickr.com.*/)) {
      return setState('flickr');
    }

    if (e.currentTarget.value.match(/http[s]*:\/\/vimeo.com.*/)) {
      return setState('vimeo');
    }

    return setState(null);
  }, []);

  return <div className="flex border rounded-sm px-4 w-full h-8 my-4 space-x-2">
    <img
      src={flickrImg}
      alt="flickr"
      className={`
        ${state === "flickr"
          ? "opacity-100"
          : "opacity-10"
        }
          transition-opacity
          duration-200
        `
      }
    />
    <img
      src={vimeoImg}
      alt="vimeo"
      className={`
        ${state === "vimeo"
          ? "opacity-100"
          : "opacity-10"
        }
      transition-opacity
      duration-200
        `
      }
    />
    <input
      ref={inputRef}
      type="url"
      className="flex-1 outline-none"
      required
      placeholder="Flickr or Vimeo URL"
      pattern=".*(flickr.com|vimeo.com).*"
      onChange={handleCheckLink}
    />
  </div>;
});

export default FlickrVimeoInput;
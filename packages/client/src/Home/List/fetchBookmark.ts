const fetchBookmark = async (url: string) => {
  // proxy that fix cors issue
  const proxy = `${import.meta?.env?.VITE_PROXY_API || "http://localhost:4000"
    }/?url=${encodeURIComponent(url)}`;;;

  const response = await fetch(proxy, {
    headers: {
      "Content-type": "application/json",
    }
  });

  if (response.status !== 200) {
    throw new Error("An error occured");
  }

  return response.json();
};

export default fetchBookmark;
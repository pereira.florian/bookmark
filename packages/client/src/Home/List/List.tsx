import React from 'react';
import { useParams } from "react-router-dom";
import Bookmark from './Line';
import { useBookmarks } from '../../BookmarkProvider';
import Pagination, { usePagination } from './Pagination';

const List = () => {
  let { page = 1 } = useParams<{ page: string; }>();
  const { bookmarks, remove } = useBookmarks();
  const [list, totalPages, prev, next] = usePagination(bookmarks, +page);

  if (bookmarks.length === 0) {
    return null;
  }

  return <div className="w-full h-full my-8">
    <div className="mb-8">
      <Header key="header" />
      <>{list.map((url, i) => <Bookmark key={url} url={url} remove={() => remove(i)} />)}</>
    </div>
    {totalPages > 1 && <Pagination currentPage={+page} totalPages={totalPages} prev={prev} next={next} />}
  </div>;
};

export default List;

const Header = () => <div className="grid grid-cols-5 gap-4 text-gray-600 text-base font-semibold px-1 py-2">
  <div>URL</div>
  <div>Title</div>
  <div>Author</div>
  <div className="truncate justify-self-end">Added at</div>
  <div></div>
</div>;

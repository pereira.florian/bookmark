
import { jest } from '@jest/globals';
import * as React from "react";
import { MemoryRouter } from 'react-router-dom';
import { render } from "@testing-library/react";
import Line from "./Line";
import { act } from 'react-dom/test-utils';

// @ts-ignore
global.fetch = jest.fn().mockReturnValue(Promise.resolve({
  status: 200,
  json: () => Promise.resolve({
    title: "ok"
  })
}));


it('Should only fetch once', async () => {
  const promise = Promise.resolve();
  const { rerender } = render(<Line url="https://www.flickr.com/any/link" remove={() => undefined} />, { wrapper: MemoryRouter });
  rerender(<Line url="https://www.flickr.com/any/link" remove={() => undefined} />);
  expect(fetch).toHaveBeenCalledWith("http://localhost:4000/?url=https%3A%2F%2Fwww.flickr.com%2Fany%2Flink", {
    headers: {
      "Content-type": "application/json",
    }
  });

  expect(fetch).toHaveBeenCalledTimes(1);
  await act(() => promise);
});
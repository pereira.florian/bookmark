import React from 'react';
import { Link } from "react-router-dom";
import useLocalStorage from '../../useLocalStorage';
import useSlugify from '../../useSlugify';
import ExternalLinkIcon from '../../ExternalLinkIcon';
import fetchBookmark from './fetchBookmark';
import type { Bookmark } from '../../Bookmark.d';

type Props = {
  url: string,
  remove: () => void,
};

const Line: React.FC<Props> = ({ url, remove }) => {
  const slug = useSlugify(url);
  const [bookmark, setBookmark, removeBookmarkDetails] = useLocalStorage<Bookmark>(slug, () => ({ id: slug, url, added_at: new Date().toISOString().slice(0, 10) }));
  const [err, setError] = React.useState(false);

  const handleRemove = React.useCallback(() => {
    removeBookmarkDetails();
    remove();
  }, []);

  React.useEffect(() => {
    if (bookmark.title !== undefined) return;

    fetchBookmark(url)
      .then((details) => setBookmark((prev: Bookmark) => ({ ...prev, ...details })))
      .catch(_ => {
        setError(true);
        setTimeout(handleRemove, 1500);
      });
  }, [url, setBookmark]);



  const { title, author_name, added_at } = bookmark;

  return <div className={`grid grid-cols-5 gap-4 text-gray-500 text-sm p-1 hover:bg-blue-50 ${err ? "border border-red-500 bg-red-400" : ""}`}>
    <div className="bg-blue-500 rounded-sm text-white gird place-items-center flex">
      <div className="hidden sm:block w-full truncate px-2">
        <a href={url} target="_blank">{url}</a>
      </div>
      <ExternalLinkIcon className="m-auto w-4 h-4 sm:hidden" />
    </div>
    <div className="truncate text-base font-semibold text-gray-600">{title}</div>
    <div className="truncate">{author_name}</div>
    <div className="truncate justify-self-end">{added_at}</div>
    <div className="truncate flex justify-self-end space-x-4 items-center">
      <EditLink to={`/${bookmark.id}/edit`} />
      <RemoveButton onClick={handleRemove} />
    </div>
  </div>;
};

export default React.memo(Line, (prev, next) => prev.url === next.url);

type EditProps = {
  to: string,
};

const EditLink: React.FC<EditProps> = ({ to }) => (
  <Link to={to} title="edit">
    <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
    </svg>
  </Link>
);


type RemoveProps = {
  onClick: () => void,
};

const RemoveButton: React.FC<RemoveProps> = ({ onClick }) => (
  <button type="button" onClick={onClick} title="remove">
    <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
    </svg>
  </button>
);


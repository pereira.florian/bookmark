import React from 'react';
import { Link } from "react-router-dom";

type Props = {
  currentPage: number,
  totalPages: number,
  prev?: boolean,
  next?: boolean,
};

const Pagination: React.FC<Props> = ({ currentPage = 1, totalPages = 1, prev = false, next = false }) => (
  <div className="flex justify-between text-sm text-gray-500">
    {prev ?
      <div className="flex">
        <Link to="/" title="First page"><GoFirstIcon /></Link>
        <Link to={`/${currentPage - 1}`} title="Previous page"><GoPrevIcon /></Link>
      </div>
      : <div />}
    <div>{currentPage}/{totalPages}</div>
    {next ?
      <div className="flex">
        <Link to={`/${currentPage + 1}`} title="Next page"><GoNextIcon /></Link>
        <Link to={`/${totalPages}`} title="Last page"><GoLastIcon /></Link>
      </div>
      : <div />}
  </div>
);

export default Pagination;

export const usePagination = <T extends {}>(list: T[], page: number): [T[], number, boolean, boolean] => React.useMemo(() => {
  const perPage = 10;

  if (list.length < perPage) {
    return [list, 0, false, false];
  }

  const pages = Math.ceil(list.length / perPage);
  const prev = page !== 1;
  const next = pages > page;

  return [list.slice((page - 1) * 10, page * 10), pages, prev, next];
}, [page, list.length]);


const GoFirstIcon = () => <svg className="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
</svg>;

const GoPrevIcon = () => <svg className="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
</svg>;

const GoNextIcon = () => <svg className="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
</svg>;

const GoLastIcon = () => <svg className="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M13 5l7 7-7 7M5 5l7 7-7 7" />
</svg>;
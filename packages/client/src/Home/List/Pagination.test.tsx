import * as React from "react";
import { MemoryRouter } from 'react-router-dom';
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Pagination from "./Pagination";

it("Display nav buttons", () => {
  const { asFragment } = render(<Pagination currentPage={2} totalPages={10} prev next />, { wrapper: MemoryRouter });
  expect(asFragment()).toMatchSnapshot();
});

it("Do not display nav buttons", () => {
  const { asFragment } = render(<Pagination currentPage={1} totalPages={10} />, { wrapper: MemoryRouter });
  expect(asFragment()).toMatchSnapshot();
});

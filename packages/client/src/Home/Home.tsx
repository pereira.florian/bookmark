import React from 'react';
import List from './List';
import NewBookmark from './NewBookmark';

const Home = () => {

  return <>
    <List />
    <NewBookmark />
  </>;
};

export default Home;
